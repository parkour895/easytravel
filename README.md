##Back-end part of my Graduate work.

Multitravel web app, which include Google Transit Api, Skyscanner Api and Aviation Edge Api.

![Scheme](images/1.jpg)
![Scheme](images/2.jpg)
![Scheme](images/3.jpg)

# easy-travel-back-end

Spring version - 4.3.4.RELEASE

DTO ganereted with: http://www.jsonschema2pojo.org/

Https through HttpClient("org.apache.httpcomponents")


## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.