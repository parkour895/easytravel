package com.khomenko.easyTravel.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.*;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;

import javafx.util.converter.DateTimeStringConverter;
import javafx.util.converter.LocalDateTimeStringConverter;
import org.joda.time.DateTime;
import org.joda.time.ReadableDateTime;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.datetime.joda.JodaDateTimeFormatAnnotationFormatterFactory;
import org.springframework.format.datetime.joda.ReadableInstantPrinter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class GoogleMapsService {

    private static final Logger LOG = Logger.getLogger(GoogleMapsService.class.getName());

    @NotNull
    @Value("${config.google.apikey}")
    private String apikey;

    private GeoApiContext context;

    private PlacesApi placesApi;


    private void initializeGeoApiContext(){
        LOG.log(Level.INFO, "Initializing context");
        LOG.log(Level.INFO, "Initializing context apikey = " + apikey);
        context=new GeoApiContext.Builder().apiKey(apikey).build();
    }

    public void getNearestAirport(String place){
        initializeGeoApiContext();
        try {
            LOG.log(Level.INFO, "after initializing, getNearestAirport geocode");
            GeocodingResult[] results= GeocodingApi.geocode(context,"biggest airport near "+place).await();
            Gson gson=new GsonBuilder().setPrettyPrinting().create();
            System.out.print(gson.toJson(results[0].addressComponents));
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LatLng getLatLngByAddress(String placeAddress){
        initializeGeoApiContext();
        LatLng latLng=null;
        try {
            LOG.log(Level.INFO, "after initializing, getNearestAirport geocode");
            GeocodingResult[] results= GeocodingApi.geocode(context,placeAddress).await();

            latLng=results[0].geometry.location;

            Gson gson=new GsonBuilder().setPrettyPrinting().create();
            LOG.log(Level.INFO, "result data " + gson.toJson(results[0]));
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return latLng;
    }

    public DirectionsResult getGroundDirection(String origin, String destination,String dateTime, Boolean isArrivalTime){
        initializeGeoApiContext();
       // DateTimeFormatter dateTimeFormatter=new DateTimeFormatterBuilder().toFormatter();


            DateTime jodaDateTime=DateTime.parse(dateTime, ISODateTimeFormat.dateTimeParser());//TODO: можуть бути проблеми з датою в місце прибуття, часові пояси
            DirectionsResult directionsResult;

            if(isArrivalTime){
                directionsResult=DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(origin).destination(destination).arrivalTime(jodaDateTime).awaitIgnoreError();
            }else
                directionsResult=DirectionsApi.newRequest(context).mode(TravelMode.TRANSIT).origin(origin).destination(destination).departureTime(jodaDateTime).awaitIgnoreError();

            LOG.log(Level.INFO, "directionsResult "+origin+"->"+destination+", time:"+dateTime+", isArrivalTime:"+isArrivalTime);
            Arrays.stream(directionsResult.routes).forEach(a->LOG.log(Level.INFO, a.toString()+" , "));

            return directionsResult;
    }

    public void getNearestAirportInPlacesApi(String place) throws InterruptedException, ApiException, IOException {
        initializeGeoApiContext();
        PlacesSearchResponse results=PlacesApi.textSearchQuery(context,"airport near "+place).await();
        Gson gson=new GsonBuilder().setPrettyPrinting().create();
        System.out.printf("Airport near "+place+"\n"+gson.toJson(results.results));
    }

    public void timeZoneDifference(LatLng latLng,DateTime dateTime){//TODO: not tested
       TimeZone timeZone= getTimeZone(latLng);
       dateTime.plus(timeZone.getOffset(dateTime.getMillis()));
    }

    public TimeZone getTimeZone(LatLng latLng){
        initializeGeoApiContext();
        try {
          return TimeZoneApi.getTimeZone(context,latLng).await();
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public GeoApiContext getContext() {
        return context;
    }

    public void setContext(GeoApiContext context) {
        this.context = context;
    }

}
