package com.khomenko.easyTravel.service;


import com.google.gson.Gson;
import com.khomenko.easyTravel.dto.foreign.skyscanner.SkyScannerFlight;
import com.khomenko.easyTravel.dto.foreign.skyscanner.flyquote.ComplexFlyQuote;
import com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice.FlyLivePrice;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.eclipse.jetty.util.StringUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Logger;

@Service
public class SkyScannerService {

    private static final Logger LOG = Logger.getLogger(SkyScannerService.class.getName());

    private final String USER_AGENT = "Mozilla/5.0";
    private final String ACCEPT_HEADER_JSON = "application/json";
    private final String CONTENT_TYPE = "application/x-www-form-urlencoded";

    @NotNull
    @Value("${config.skyscanner.apikey}")
    private String apikey;

    @Value("${config.skyscanner.quoutes.uri}")
    private String quoutesUri;

    @Value("${config.skyscanner.flights.prices.session.url}")
    private String livePricesUrl;

    private static HttpClient httpClient;

    private void initializeHttpClient(){
        SSLConnectionSocketFactory socketFactory = null;
        try {
            socketFactory = new SSLConnectionSocketFactory(
                    new SSLContextBuilder().loadTrustMaterial(null,
                            new TrustSelfSignedStrategy()).build());

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
    }

    public ComplexFlyQuote getComplexFlyQuote(String originPlace, String destinationPlace, LocalDate outboundPartialDate) {

        if(httpClient==null)
            initializeHttpClient();

        ComplexFlyQuote complexFlyQuote=null;

        try {
            URI uri = new URIBuilder(quoutesUri+"PL/PLN/pl-Pl/" + originPlace + "/" + destinationPlace  + "/" + outboundPartialDate.toString()).setParameter("apiKey",apikey).build();
            LOG.info("getQuotes() UriBuilder uri=" + uri);
            HttpGet httpGet = new HttpGet(uri);
            httpGet.addHeader("User-Agent", USER_AGENT);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity entity = httpResponse.getEntity();
            String responseString = EntityUtils.toString(entity, "UTF-8");
            LOG.info("getComplexFlyQuote() httpResponse: responseString: " + responseString);
            complexFlyQuote = new Gson().fromJson(responseString,ComplexFlyQuote.class);
            LOG.info("getComplexFlyQuote() httpResponse: ComplexFlyQuote was correctly deserialized.");
            return complexFlyQuote;


        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return complexFlyQuote;
    }





    public String createLiveSession(SkyScannerFlight flight, String ip){
        try {
            com.mashape.unirest.http.HttpResponse<JsonNode> response = Unirest.post(livePricesUrl)
                    .header("Content-Type", CONTENT_TYPE)
                    .header("X-Forwarded-For", ip)
                    .field("country","DE")
                    .field("currency","EUR")
                    .field("locale","pl-PL")
                    .field("locationSchema","iata")
                    .field("originplace", flight.getOriginplace())
                    .field("destinationplace",flight.getDestinationplace())
                    .field("outbounddate", flight.getOutbounddate())
                    .field("inbounddate", flight.getInbounddate())
                    .field("adults", flight.getAdults())
                    .field("children", flight.getChildren())
                    .field("infants", flight.getInfants())
                    .field("apiKey",apikey)
                    .asJson();

            if(response.getStatus()==201){
                String sessionUrl=response.getHeaders().get("Location").get(0);
                LOG.info("getComplexFlyQuote() httpResponse: responseString: " + sessionUrl);
                return sessionUrl;
            }

            if(response.getStatus()==400){
              String responseBody=response.getBody().toString();
              LOG.info("getComplexFlyQuote() httpResponse: Bad request, responseString: " + responseBody);
              throw new NullPointerException(responseBody);
            }

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e) {
          e.printStackTrace();
        }
        return "";
    }

    public FlyLivePrice  getLivePrices(String url){
        try {
            if(StringUtil.isNotBlank(url)) {
                Future<com.mashape.unirest.http.HttpResponse<JsonNode>> response =  Unirest.get(url + "?apiKey=" + apikey).asJsonAsync();
                return new Gson().fromJson(response.get().getBody().toString(), FlyLivePrice.class);
            }
            else
                throw new NullPointerException("error getLivePrices(), url mast not be empty");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }
}
