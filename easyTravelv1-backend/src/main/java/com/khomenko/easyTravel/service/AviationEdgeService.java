package com.khomenko.easyTravel.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.khomenko.easyTravel.dto.foreign.aviation.Airport;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import com.google.maps.model.LatLng;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@Service
public class AviationEdgeService {

    private static final Logger LOG=Logger.getLogger(AviationEdgeService.class.getName());

    private final String USER_AGENT = "Mozilla/5.0";

    @NotNull
    @Value("${config.aviation.edge.apikey}")
    private String apikey;

    @Value("${config.aviation.edge.nearbyUrl}")
    private String aviationEdgeUrl;

    @Value("${config.aviation.edge.distance.from.point.km}")
    private String distance;

  public List<Airport> getNearestAirportList(LatLng latLng){

      List<Airport> airports = null;

        try {
            SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
                new SSLContextBuilder().loadTrustMaterial(null,
                        new TrustSelfSignedStrategy()).build());
            HttpClient httpClient= HttpClients.custom().setSSLSocketFactory(socketFactory).build();
            URI uri= new URIBuilder(aviationEdgeUrl)
                    .addParameter("key", apikey)
                    .addParameter("lat", String.valueOf(latLng.lat))
                    .addParameter("lng", String.valueOf(latLng.lng))
                    .addParameter("distance", distance)
                    .build();
            LOG.info("getNearestAirport() UriBuilder uri="+uri);

            HttpGet httpGet = new HttpGet(uri);
            httpGet.addHeader("User-Agent", USER_AGENT);
            HttpResponse httpResponse=httpClient.execute(httpGet);
            HttpEntity entity = httpResponse.getEntity();
            String responseString = EntityUtils.toString(entity, "UTF-8");
            responseString=responseString.replaceAll("\n","");
            LOG.info("getNearestAirport() httpResponse:" + responseString.replaceAll(" ",""));
            airports = new Gson().fromJson(responseString,new TypeToken<List<Airport>>(){}.getType());
            removeRailway(airports);
            airports= Arrays.asList(airports.get(0));//TODO: перевірити апі бо вертає вокзали
            LOG.info("getNearestAirport() List<Airport> airports was correctly created");
            return airports;

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return airports;
  }

  private void removeRailway(List<Airport> airports){
    airports.removeIf(point->point.getName().contains("Railway"));
  }

//    public void getNearestAirportNative(LatLng latLng){
//        URI uri= null;
//        try {
//            uri = new URIBuilder(aviationEdgeUrl)
//                    .addParameter("key", apikey)
//                    .addParameter("lat", String.valueOf(latLng.lat))
//                    .addParameter("lng", String.valueOf(latLng.lng))
//                    .addParameter("distance", "1000")
//                    .build();
//            HttpResponse<JsonNode> jsonResponse= Unirest.get(uri.toString()).asJson();
//            System.out.printf("http response"+jsonResponse);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        } catch (UnirestException e) {
//            e.printStackTrace();
//        }
//    }
}
