package com.khomenko.easyTravel.controller;

import com.google.maps.model.DirectionsResult;
import com.google.maps.model.LatLng;
import com.khomenko.easyTravel.dto.foreign.aviation.Airport;
import com.khomenko.easyTravel.dto.foreign.skyscanner.SkyScannerFlight;
import com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice.FlyLivePrice;
import com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice.Leg;
import com.khomenko.easyTravel.dto.own.GroundDirection;
import com.khomenko.easyTravel.dto.own.Trip;
import com.khomenko.easyTravel.service.AviationEdgeService;
import com.khomenko.easyTravel.service.GoogleMapsService;
import com.khomenko.easyTravel.service.SkyScannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
public class BasicController {

	@Autowired
	GoogleMapsService googleMapsService;

	@Autowired
	AviationEdgeService aviationEdgeService;

	@Autowired
	SkyScannerService skyScannerService;

	@RequestMapping(value = "/getAddressLatLng/{address}")
	public String getUser(@PathVariable("address") String address) {
			LatLng latLng = googleMapsService.getLatLngByAddress(address);
			return latLng.toString();
	}



	// @RequestMapping(value = "/nearestairport")
	// public String getNearest(){
	// 	LatLng latLng=new LatLng(49.6766,21.7637);
	// 	aviationEdgeService.getNearestAirportList(latLng);
	// 	return "itak";
	// }

  	@CrossOrigin(origins = "http://localhost:9000")
	@RequestMapping(value = "/optimalWay", method = RequestMethod.POST)
	public FlyLivePrice getOptimalWay(@RequestBody Trip trip) {
		if (trip.getOriginCoord() == null)
			trip.setOriginCoord(googleMapsService.getLatLngByAddress(trip.getOriginAddress()));
		if (trip.getDestinationCoord() == null)
			trip.setDestinationCoord(googleMapsService.getLatLngByAddress(trip.getDestinationAddress()));
		List<Airport> originAirports = aviationEdgeService.getNearestAirportList(trip.getOriginCoord());
		List<Airport> destinationAirports = aviationEdgeService.getNearestAirportList(trip.getDestinationCoord());
		//TimeZone originTimeZone=googleMapsService.getTimeZone(trip.getOriginCoord());
		//TimeZone destinTimeZone=googleMapsService.getTimeZone(trip.getDestinationCoord());
		//List<ComplexFlyQuote> complexFlyQuoteList = new ArrayList<>();
		//LocalDate localDate = LocalDate.parse(trip.getOutbounddate(), DateTimeFormatter.ISO_LOCAL_DATE);
		for (Airport originAirport : originAirports)
			for (Airport destinationAirport : destinationAirports) {
				SkyScannerFlight skyScannerFlight = new SkyScannerFlight();//TODO: problem with date when is user from another country
				skyScannerFlight.setOriginplace(originAirport.getCode());
				skyScannerFlight.setDestinationplace(destinationAirport.getCode());
				skyScannerFlight.setOutbounddate(trip.getOutbounddate().toString());
				skyScannerFlight.setAdults(trip.getAdults());
				String livePricesUrl = skyScannerService.createLiveSession(skyScannerFlight, "192.168.56.1");//TODO: change
				FlyLivePrice livePrice = skyScannerService.getLivePrices(livePricesUrl);
				if (livePrice != null && !livePrice.getItineraries().isEmpty()) {
					List<Leg> legs = livePrice.getLegs();
					legs.forEach((leg) -> {
						DirectionsResult toOriginAirport = googleMapsService.getGroundDirection(trip.getOriginAddress(), originAirport.getName(), leg.getDeparture(), true);//TODO: local time problem
						DirectionsResult toDestinationAdd = googleMapsService.getGroundDirection(destinationAirport.getName(), trip.getDestinationAddress(), leg.getArrival(), false);//TODO: add function for local time
						GroundDirection groundDirection = new GroundDirection(toOriginAirport, toDestinationAdd);
						leg.setGroundDirection(groundDirection);
					});
				}
				return livePrice;


			}
		return null;
	}








//	private void addGroundDirectionToQuotes(List<Quote> quotes, String originAddress, String originAirport, String destinationAirport, String destinationAddress){
//		for (Quote quote: quotes)
//		googleMapsService.getGroundDirection(originAddress,originAirport,quote.getOutboundLeg().getDepartureDate());
//
//	}



//	@RequestMapping(value = "/optimalWay")
//	public String getOptimalWay(@PathVariable("trip") Trip trip) {
//		if(trip.getOriginCoord()==null)
//			trip.setOriginCoord(googleMapsService.getLatLngByAddress(trip.getOriginAddress()));
//		if(trip.getDestinationCoord()==null)
//			trip.setDestinationCoord(googleMapsService.getLatLngByAddress(trip.getDestinationAddress()));
//		List<Airport> originAirports = aviationEdgeService.getNearestAirportList(trip.getDestinationCoord());
//		List<Airport> destinationAirports = aviationEdgeService.getNearestAirportList(trip.getDestinationCoord());
//		List<ComplexFlyQuote> complexFlyQuoteList = new ArrayList<>();
//		//LocalDate localDate = LocalDate.parse(trip.getOutbounddate(), DateTimeFormatter.ISO_LOCAL_DATE);
//		for (Airport originAirport : originAirports)
//			for (Airport destinationAirport : destinationAirports) {
//				ComplexFlyQuote complexFlyQuote = skyScannerService.getComplexFlyQuote(originAirport.getCode(), destinationAirport.getCode(), trip.getOutbounddate());
//				if (complexFlyQuote != null){
//					addGroundDirectionToQuotes(complexFlyQuote.getQuotes(),trip.getOriginAddress(),originAirport.getName(),destinationAirport.getName(),trip.getDestinationAddress());
//					complexFlyQuoteList.add(complexFlyQuote);
//				}
//			}
//		return "";
//	}

}
