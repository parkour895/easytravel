package com.khomenko.easyTravel.dto.foreign.skyscanner.flyquote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.model.DirectionsRoute;

public class Quote {

    @SerializedName("QuoteId")
    @Expose
    private Integer quoteId;
    @SerializedName("QuoteDateTime")
    @Expose
    private String quoteDateTime;
    @SerializedName("MinPrice")
    @Expose
    private Integer minPrice;
    @SerializedName("OutboundLeg")
    @Expose
    private OutboundLeg outboundLeg;
    @SerializedName("Direct")
    @Expose
    private Boolean direct;
    @SerializedName("DirectionToAirport")
    @Expose
    private DirectionsRoute directionsToAirport;
    @SerializedName("DirectionFromAirport")
    @Expose
    private DirectionsRoute directionsFromAirport;

    public Integer getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(Integer quoteId) {
        this.quoteId = quoteId;
    }

    public String getQuoteDateTime() {
        return quoteDateTime;
    }

    public void setQuoteDateTime(String quoteDateTime) {
        this.quoteDateTime = quoteDateTime;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public OutboundLeg getOutboundLeg() {
        return outboundLeg;
    }

    public void setOutboundLeg(OutboundLeg outboundLeg) {
        this.outboundLeg = outboundLeg;
    }

    public Boolean getDirect() {
        return direct;
    }

    public void setDirect(Boolean direct) {
        this.direct = direct;
    }

    public DirectionsRoute getDirectionsToAirport() {
        return directionsToAirport;
    }

    public void setDirectionsToAirport(DirectionsRoute directionsToAirport) {
        this.directionsToAirport = directionsToAirport;
    }

    public DirectionsRoute getDirectionsFromAirport() {
        return directionsFromAirport;
    }

    public void setDirectionsFromAirport(DirectionsRoute directionsFromAirport) {
        this.directionsFromAirport = directionsFromAirport;
    }
}
