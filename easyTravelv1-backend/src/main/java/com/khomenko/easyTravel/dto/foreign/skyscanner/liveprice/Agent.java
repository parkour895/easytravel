
package com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Agent {

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("OptimisedForMobile")
    @Expose
    private Boolean optimisedForMobile;
    @SerializedName("BookingNumber")
    @Expose
    private String bookingNumber;
    @SerializedName("Name")
    @Expose
    private String name;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getOptimisedForMobile() {
        return optimisedForMobile;
    }

    public void setOptimisedForMobile(Boolean optimisedForMobile) {
        this.optimisedForMobile = optimisedForMobile;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("type", type).append("imageUrl", imageUrl).append("id", id).append("optimisedForMobile", optimisedForMobile).append("bookingNumber", bookingNumber).append("name", name).toString();
    }

}
