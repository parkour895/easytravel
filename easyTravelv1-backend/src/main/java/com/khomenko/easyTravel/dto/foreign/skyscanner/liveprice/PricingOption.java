
package com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PricingOption {

    @SerializedName("DeeplinkUrl")
    @Expose
    private String deeplinkUrl;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("Agents")
    @Expose
    private List<Integer> agents = null;
    @SerializedName("QuoteAgeInMinutes")
    @Expose
    private Integer quoteAgeInMinutes;

    public String getDeeplinkUrl() {
        return deeplinkUrl;
    }

    public void setDeeplinkUrl(String deeplinkUrl) {
        this.deeplinkUrl = deeplinkUrl;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Integer> getAgents() {
        return agents;
    }

    public void setAgents(List<Integer> agents) {
        this.agents = agents;
    }

    public Integer getQuoteAgeInMinutes() {
        return quoteAgeInMinutes;
    }

    public void setQuoteAgeInMinutes(Integer quoteAgeInMinutes) {
        this.quoteAgeInMinutes = quoteAgeInMinutes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("deeplinkUrl", deeplinkUrl).append("price", price).append("agents", agents).append("quoteAgeInMinutes", quoteAgeInMinutes).toString();
    }

}
