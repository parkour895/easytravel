
package com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Itinerary {

    @SerializedName("BookingDetailsLink")
    @Expose
    private BookingDetailsLink bookingDetailsLink;
    @SerializedName("OutboundLegId")
    @Expose
    private String outboundLegId;
    @SerializedName("PricingOptions")
    @Expose
    private List<PricingOption> pricingOptions = null;

    public BookingDetailsLink getBookingDetailsLink() {
        return bookingDetailsLink;
    }

    public void setBookingDetailsLink(BookingDetailsLink bookingDetailsLink) {
        this.bookingDetailsLink = bookingDetailsLink;
    }

    public String getOutboundLegId() {
        return outboundLegId;
    }

    public void setOutboundLegId(String outboundLegId) {
        this.outboundLegId = outboundLegId;
    }

    public List<PricingOption> getPricingOptions() {
        return pricingOptions;
    }

    public void setPricingOptions(List<PricingOption> pricingOptions) {
        this.pricingOptions = pricingOptions;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("bookingDetailsLink", bookingDetailsLink).append("outboundLegId", outboundLegId).append("pricingOptions", pricingOptions).toString();
    }

}
