package com.khomenko.easyTravel.dto.own;

import com.google.maps.model.LatLng;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;

import java.time.LocalDate;


public class Trip{
    @NotNull
    private String originAddress;
    @NotNull
    private String destinationAddress;

    private LatLng originCoord;

    private LatLng destinationCoord;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate outbounddate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate inbounddate;

    @NotNull
    private String adults;

    private String children;

    private String infants;

    public String getOriginAddress() {
        return originAddress;
    }

    public void setOriginAddress(String originAddress) {
        this.originAddress = originAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public LatLng getOriginCoord() {
        return originCoord;
    }

    public void setOriginCoord(LatLng originCoord) {
        this.originCoord = originCoord;
    }

    public LatLng getDestinationCoord() {
        return destinationCoord;
    }

    public void setDestinationCoord(LatLng destinationCoord) {
        this.destinationCoord = destinationCoord;
    }

    public LocalDate getOutbounddate() {
        return outbounddate;
    }

    public void setOutbounddate(LocalDate outbounddate) {
        this.outbounddate = outbounddate;
    }

    public LocalDate getInbounddate() {
        return inbounddate;
    }

    public void setInbounddate(LocalDate inbounddate) {
        this.inbounddate = inbounddate;
    }

    public String getAdults() {
        return adults;
    }

    public void setAdults(String adults) {
        this.adults = adults;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getInfants() {
        return infants;
    }

    public void setInfants(String infants) {
        this.infants = infants;
    }

}
