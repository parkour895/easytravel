
package com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class FlightNumber {

    @SerializedName("CarrierId")
    @Expose
    private Integer carrierId;
    @SerializedName("FlightNumber")
    @Expose
    private String flightNumber;

    public Integer getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(Integer carrierId) {
        this.carrierId = carrierId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("carrierId", carrierId).append("flightNumber", flightNumber).toString();
    }

}
