
package com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class FlyLivePrice {

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Carriers")
    @Expose
    private List<Carrier> carriers = null;
    @SerializedName("Legs")
    @Expose
    private List<Leg> legs = null;
    @SerializedName("Itineraries")
    @Expose
    private List<Itinerary> itineraries = null;
    @SerializedName("Query")
    @Expose
    private Query query;
    @SerializedName("SessionKey")
    @Expose
    private String sessionKey;
    @SerializedName("Agents")
    @Expose
    private List<Agent> agents = null;
    @SerializedName("Segments")
    @Expose
    private List<Segment> segments = null;
    @SerializedName("Places")
    @Expose
    private List<Place> places = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Carrier> getCarriers() {
        return carriers;
    }

    public void setCarriers(List<Carrier> carriers) {
        this.carriers = carriers;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    public List<Itinerary> getItineraries() {
        return itineraries;
    }

    public void setItineraries(List<Itinerary> itineraries) {
        this.itineraries = itineraries;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public List<Agent> getAgents() {
        return agents;
    }

    public void setAgents(List<Agent> agents) {
        this.agents = agents;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("carriers", carriers).append("legs", legs).append("itineraries", itineraries).append("query", query).append("sessionKey", sessionKey).append("agents", agents).append("segments", segments).append("places", places).toString();
    }

}
