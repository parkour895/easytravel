package com.khomenko.easyTravel.dto.own;

import com.google.maps.model.DirectionsResult;

public class GroundDirection {

    private DirectionsResult originDirection;

    private DirectionsResult destinDirection;

    public GroundDirection( DirectionsResult originDirection, DirectionsResult destinDirection) {
        this.originDirection = originDirection;
        this.destinDirection = destinDirection;
    }

    public DirectionsResult getOriginDirection() {
        return originDirection;
    }

    public void setOriginDirection(DirectionsResult originDirection) {
        this.originDirection = originDirection;
    }

    public DirectionsResult getDestinDirection() {
        return destinDirection;
    }

    public void setDestinDirection(DirectionsResult destinDirection) {
        this.destinDirection = destinDirection;
    }
}
