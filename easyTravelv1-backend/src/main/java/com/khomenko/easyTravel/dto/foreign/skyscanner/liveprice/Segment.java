
package com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Segment {

    @SerializedName("Directionality")
    @Expose
    private String directionality;
    @SerializedName("OriginStation")
    @Expose
    private Integer originStation;
    @SerializedName("DepartureDateTime")
    @Expose
    private String departureDateTime;
    @SerializedName("ArrivalDateTime")
    @Expose
    private String arrivalDateTime;
    @SerializedName("JourneyMode")
    @Expose
    private String journeyMode;
    @SerializedName("DestinationStation")
    @Expose
    private Integer destinationStation;
    @SerializedName("OperatingCarrier")
    @Expose
    private Integer operatingCarrier;
    @SerializedName("FlightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("Duration")
    @Expose
    private Integer duration;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Carrier")
    @Expose
    private Integer carrier;
    @SerializedName("DecimalSeparator")
    @Expose
    private String decimalSeparator;
    @SerializedName("ThousandsSeparator")
    @Expose
    private String thousandsSeparator;
    @SerializedName("SymbolOnLeft")
    @Expose
    private Boolean symbolOnLeft;
    @SerializedName("SpaceBetweenAmountAndSymbol")
    @Expose
    private Boolean spaceBetweenAmountAndSymbol;
    @SerializedName("Symbol")
    @Expose
    private String symbol;
    @SerializedName("DecimalDigits")
    @Expose
    private Integer decimalDigits;
    @SerializedName("Code")
    @Expose
    private String code;
    @SerializedName("RoundingCoefficient")
    @Expose
    private Integer roundingCoefficient;

    public String getDirectionality() {
        return directionality;
    }

    public void setDirectionality(String directionality) {
        this.directionality = directionality;
    }

    public Integer getOriginStation() {
        return originStation;
    }

    public void setOriginStation(Integer originStation) {
        this.originStation = originStation;
    }

    public String getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public String getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(String arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getJourneyMode() {
        return journeyMode;
    }

    public void setJourneyMode(String journeyMode) {
        this.journeyMode = journeyMode;
    }

    public Integer getDestinationStation() {
        return destinationStation;
    }

    public void setDestinationStation(Integer destinationStation) {
        this.destinationStation = destinationStation;
    }

    public Integer getOperatingCarrier() {
        return operatingCarrier;
    }

    public void setOperatingCarrier(Integer operatingCarrier) {
        this.operatingCarrier = operatingCarrier;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCarrier() {
        return carrier;
    }

    public void setCarrier(Integer carrier) {
        this.carrier = carrier;
    }

    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(String decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }

    public String getThousandsSeparator() {
        return thousandsSeparator;
    }

    public void setThousandsSeparator(String thousandsSeparator) {
        this.thousandsSeparator = thousandsSeparator;
    }

    public Boolean getSymbolOnLeft() {
        return symbolOnLeft;
    }

    public void setSymbolOnLeft(Boolean symbolOnLeft) {
        this.symbolOnLeft = symbolOnLeft;
    }

    public Boolean getSpaceBetweenAmountAndSymbol() {
        return spaceBetweenAmountAndSymbol;
    }

    public void setSpaceBetweenAmountAndSymbol(Boolean spaceBetweenAmountAndSymbol) {
        this.spaceBetweenAmountAndSymbol = spaceBetweenAmountAndSymbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getDecimalDigits() {
        return decimalDigits;
    }

    public void setDecimalDigits(Integer decimalDigits) {
        this.decimalDigits = decimalDigits;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getRoundingCoefficient() {
        return roundingCoefficient;
    }

    public void setRoundingCoefficient(Integer roundingCoefficient) {
        this.roundingCoefficient = roundingCoefficient;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("directionality", directionality).append("originStation", originStation).append("departureDateTime", departureDateTime).append("arrivalDateTime", arrivalDateTime).append("journeyMode", journeyMode).append("destinationStation", destinationStation).append("operatingCarrier", operatingCarrier).append("flightNumber", flightNumber).append("duration", duration).append("id", id).append("carrier", carrier).append("decimalSeparator", decimalSeparator).append("thousandsSeparator", thousandsSeparator).append("symbolOnLeft", symbolOnLeft).append("spaceBetweenAmountAndSymbol", spaceBetweenAmountAndSymbol).append("symbol", symbol).append("decimalDigits", decimalDigits).append("code", code).append("roundingCoefficient", roundingCoefficient).toString();
    }

}
