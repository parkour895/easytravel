
package com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Query {

    @SerializedName("Locale")
    @Expose
    private String locale;
    @SerializedName("CabinClass")
    @Expose
    private String cabinClass;
    @SerializedName("Currency")
    @Expose
    private String currency;
    @SerializedName("LocationSchema")
    @Expose
    private String locationSchema;
    @SerializedName("GroupPricing")
    @Expose
    private Boolean groupPricing;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Adults")
    @Expose
    private Integer adults;
    @SerializedName("Infants")
    @Expose
    private Integer infants;
    @SerializedName("Children")
    @Expose
    private Integer children;
    @SerializedName("OutboundDate")
    @Expose
    private String outboundDate;
    @SerializedName("OriginPlace")
    @Expose
    private String originPlace;
    @SerializedName("DestinationPlace")
    @Expose
    private String destinationPlace;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getCabinClass() {
        return cabinClass;
    }

    public void setCabinClass(String cabinClass) {
        this.cabinClass = cabinClass;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLocationSchema() {
        return locationSchema;
    }

    public void setLocationSchema(String locationSchema) {
        this.locationSchema = locationSchema;
    }

    public Boolean getGroupPricing() {
        return groupPricing;
    }

    public void setGroupPricing(Boolean groupPricing) {
        this.groupPricing = groupPricing;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    public Integer getInfants() {
        return infants;
    }

    public void setInfants(Integer infants) {
        this.infants = infants;
    }

    public Integer getChildren() {
        return children;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }

    public String getOutboundDate() {
        return outboundDate;
    }

    public void setOutboundDate(String outboundDate) {
        this.outboundDate = outboundDate;
    }

    public String getOriginPlace() {
        return originPlace;
    }

    public void setOriginPlace(String originPlace) {
        this.originPlace = originPlace;
    }

    public String getDestinationPlace() {
        return destinationPlace;
    }

    public void setDestinationPlace(String destinationPlace) {
        this.destinationPlace = destinationPlace;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("locale", locale).append("cabinClass", cabinClass).append("currency", currency).append("locationSchema", locationSchema).append("groupPricing", groupPricing).append("country", country).append("adults", adults).append("infants", infants).append("children", children).append("outboundDate", outboundDate).append("originPlace", originPlace).append("destinationPlace", destinationPlace).toString();
    }

}
