package com.khomenko.easyTravel.dto.foreign.skyscanner.flyquote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ComplexFlyQuote {
    @SerializedName("Carriers")
    @Expose
    private List<QtCarrier> qtCarriers = null;
    @SerializedName("Quotes")
    @Expose
    private List<Quote> quotes = null;
    @SerializedName("Currencies")
    @Expose
    private List<QtCurrency> currencies = null;
    @SerializedName("Places")
    @Expose
    private List<QtPlace> qtPlaces = null;

    public List<QtCarrier> getQtCarriers() {
        return qtCarriers;
    }

    public void setQtCarriers(List<QtCarrier> qtCarriers) {
        this.qtCarriers = qtCarriers;
    }

    public List<Quote> getQuotes() {
        return quotes;
    }

    public void setQuotes(List<Quote> quotes) {
        this.quotes = quotes;
    }

    public List<QtCurrency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<QtCurrency> currencies) {
        this.currencies = currencies;
    }

    public List<QtPlace> getQtPlaces() {
        return qtPlaces;
    }

    public void setQtPlaces(List<QtPlace> qtPlaces) {
        this.qtPlaces = qtPlaces;
    }

}
