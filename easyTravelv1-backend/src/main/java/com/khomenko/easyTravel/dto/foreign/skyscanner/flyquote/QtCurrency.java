package com.khomenko.easyTravel.dto.foreign.skyscanner.flyquote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QtCurrency {

    @SerializedName("DecimalSeparator")
    @Expose
    private String decimalSeparator;
    @SerializedName("ThousandsSeparator")
    @Expose
    private String thousandsSeparator;
    @SerializedName("SymbolOnLeft")
    @Expose
    private Boolean symbolOnLeft;
    @SerializedName("SpaceBetweenAmountAndSymbol")
    @Expose
    private Boolean spaceBetweenAmountAndSymbol;
    @SerializedName("Symbol")
    @Expose
    private String symbol;
    @SerializedName("DecimalDigits")
    @Expose
    private Integer decimalDigits;
    @SerializedName("Code")
    @Expose
    private String code;
    @SerializedName("RoundingCoefficient")
    @Expose
    private Integer roundingCoefficient;

    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(String decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }

    public String getThousandsSeparator() {
        return thousandsSeparator;
    }

    public void setThousandsSeparator(String thousandsSeparator) {
        this.thousandsSeparator = thousandsSeparator;
    }

    public Boolean getSymbolOnLeft() {
        return symbolOnLeft;
    }

    public void setSymbolOnLeft(Boolean symbolOnLeft) {
        this.symbolOnLeft = symbolOnLeft;
    }

    public Boolean getSpaceBetweenAmountAndSymbol() {
        return spaceBetweenAmountAndSymbol;
    }

    public void setSpaceBetweenAmountAndSymbol(Boolean spaceBetweenAmountAndSymbol) {
        this.spaceBetweenAmountAndSymbol = spaceBetweenAmountAndSymbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getDecimalDigits() {
        return decimalDigits;
    }

    public void setDecimalDigits(Integer decimalDigits) {
        this.decimalDigits = decimalDigits;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getRoundingCoefficient() {
        return roundingCoefficient;
    }

    public void setRoundingCoefficient(Integer roundingCoefficient) {
        this.roundingCoefficient = roundingCoefficient;
    }

}
