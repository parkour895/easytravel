package com.khomenko.easyTravel.dto.foreign.skyscanner;

import javax.validation.constraints.NotNull;


public class SkyScannerFlight {
    @NotNull
    private String country = "PL";
    @NotNull
    private String currency = "PLN";
    @NotNull
    private String locationSchema = "iata";
    @NotNull
    private String locale = "PL";
    @NotNull
    private String originplace;
    @NotNull
    private String destinationplace;
    @NotNull
    private String outbounddate;

    private String inbounddate;
    @NotNull
    private String adults;

    private String children;

    private String infants;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLocationSchema() {
        return locationSchema;
    }

    public void setLocationSchema(String locationSchema) {
        this.locationSchema = locationSchema;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getOriginplace() {
        return originplace;
    }

    public void setOriginplace(String originplace) {
        this.originplace = originplace;
    }

    public String getDestinationplace() {
        return destinationplace;
    }

    public void setDestinationplace(String destinationplace) {
        this.destinationplace = destinationplace;
    }

    public String getOutbounddate() {
        return outbounddate;
    }

    public void setOutbounddate(String outbounddate) {
        this.outbounddate = outbounddate;
    }

    public String getInbounddate() {
        return inbounddate;
    }

    public void setInbounddate(String inbounddate) {
        this.inbounddate = inbounddate;
    }

    public String getAdults() {
        return adults;
    }

    public void setAdults(String adults) {
        this.adults = adults;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getInfants() {
        return infants;
    }

    public void setInfants(String infants) {
        this.infants = infants;
    }

//    public List<NameValuePair> getNameValuePair(){
//        List<NameValuePair> nameValuePairs=new ArrayList<>();
//        for(Field field: this.getClass().getFields()){
//            nameValuePairs.add(new BasicNameValuePair(field.getName(),String.valueOf(field)));
//        }
//        return nameValuePairs;
//    }
}


