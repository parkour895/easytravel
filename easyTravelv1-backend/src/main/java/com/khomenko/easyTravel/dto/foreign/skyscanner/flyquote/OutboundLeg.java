package com.khomenko.easyTravel.dto.foreign.skyscanner.flyquote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OutboundLeg {

    @SerializedName("CarrierIds")
    @Expose
    private List<Integer> carrierIds = null;
    @SerializedName("DepartureDate")
    @Expose
    private String departureDate;
    @SerializedName("OriginId")
    @Expose
    private Integer originId;
    @SerializedName("DestinationId")
    @Expose
    private Integer destinationId;

    public List<Integer> getCarrierIds() {
        return carrierIds;
    }

    public void setCarrierIds(List<Integer> carrierIds) {
        this.carrierIds = carrierIds;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public Integer getOriginId() {
        return originId;
    }

    public void setOriginId(Integer originId) {
        this.originId = originId;
    }

    public Integer getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Integer destinationId) {
        this.destinationId = destinationId;
    }

}