package com.khomenko.easyTravel;

import com.khomenko.easyTravel.service.AviationEdgeService;
import com.khomenko.easyTravel.service.GoogleMapsService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    private static final Logger LOG = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


//    @Bean
//    public GoogleMapsService getGoogleService(){
//        GoogleMapsService service = new GoogleMapsService();
//        LOG.log(Level.INFO,"Application class getGoogleService after new GoogleMapsService");
//        return service;
//    }
//
//    @Bean
//    public AviationEdgeService getAviatioEdgeService(){
//        AviationEdgeService service=new AviationEdgeService();
//        LOG.info("Application class getAviatioEdgeService()");
//        return service;
//    }
}
