import com.khomenko.easyTravel.Application;
import com.khomenko.easyTravel.dto.foreign.skyscanner.SkyScannerFlight;
import com.khomenko.easyTravel.dto.foreign.skyscanner.flyquote.OutboundLeg;
import com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice.FlyLivePrice;
import com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice.Itinerary;
import com.khomenko.easyTravel.dto.foreign.skyscanner.liveprice.Leg;
import com.khomenko.easyTravel.service.SkyScannerService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class SkyScannerServiceTest {
//    @TestConfiguration
//    static class SkyScannerServiceTestConfiguration{
//        @Bean
//        public SkyScannerService getSkyScanerService(){
//            SkyScannerService skyScannerService=new SkyScannerService();
//            return skyScannerService;
//        }
//    }

    @Autowired
    private SkyScannerService skyScannerService;

    @Test
    public void getSky(){
        LocalDate localDate=LocalDate.parse("2018-04-11", DateTimeFormatter.ISO_LOCAL_DATE);
        skyScannerService.getComplexFlyQuote("RZE","WAW", localDate);
    }
    @Test
    public void checkLivePrice(){
        SkyScannerFlight skyScannerFlight=new SkyScannerFlight();
        skyScannerFlight.setOriginplace("RZE");
        skyScannerFlight.setDestinationplace("WAW");
        skyScannerFlight.setOutbounddate("2018-04-11");
        skyScannerFlight.setAdults("1");
        String livePricesUrl=skyScannerService.createLiveSession(skyScannerFlight, "192.168.56.1");
        FlyLivePrice livePrice= skyScannerService.getLivePrices(livePricesUrl);
        Assert.assertTrue(livePrice!=null);
    }

    @Test
    public void createLiveSession(){//not done
        SkyScannerFlight skyScannerFlight=new SkyScannerFlight();
        skyScannerFlight.setOriginplace("RZE");
        skyScannerFlight.setDestinationplace("WAW");
        skyScannerFlight.setOutbounddate("2018-04-11");
        skyScannerFlight.setAdults("1");
        String livePricesUrl=skyScannerService.createLiveSession(skyScannerFlight, "192.168.56.1");
        FlyLivePrice livePrice= skyScannerService.getLivePrices(livePricesUrl);
        if(livePrice!=null&&!livePrice.getItineraries().isEmpty()){
            List<Leg> legs= livePrice.getLegs();
            legs.forEach((leg) -> {
                leg.getDeparture();

            } );
        }
    }

}
