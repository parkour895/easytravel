import com.google.maps.model.LatLng;
import com.khomenko.easyTravel.Application;
import com.khomenko.easyTravel.dto.foreign.aviation.Airport;
import com.khomenko.easyTravel.dto.foreign.skyscanner.flyquote.ComplexFlyQuote;
import com.khomenko.easyTravel.service.AviationEdgeService;
import com.khomenko.easyTravel.service.GoogleMapsService;
import com.khomenko.easyTravel.service.SkyScannerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ComplexServicesTest {
    @Autowired
    private AviationEdgeService aviationEdgeService;

    @Autowired
    private GoogleMapsService googleMapsService;

    @Autowired
    private SkyScannerService skyScannerService;


    @Test
    public void getOptimalWay() {
        LatLng fromCrd = googleMapsService.getLatLngByAddress("debicka 77, rzeszow");
        LatLng toCrd = googleMapsService.getLatLngByAddress("Mostowa 3, warka");
        List<Airport> originAirports = aviationEdgeService.getNearestAirportList(fromCrd);
        List<Airport> destinationAirports = aviationEdgeService.getNearestAirportList(toCrd);
        List<ComplexFlyQuote> complexFlyQuoteList =new ArrayList<>();
        LocalDate localDate=LocalDate.parse("2018-06-11", DateTimeFormatter.ISO_LOCAL_DATE);
        for(Airport originAirport: originAirports)
            for (Airport destinationAirport: destinationAirports){
                ComplexFlyQuote complexFlyQuote =skyScannerService.getComplexFlyQuote(originAirport.getCode(),destinationAirport.getCode(),localDate);
                if(complexFlyQuote!=null)
                complexFlyQuoteList.add(complexFlyQuote);
            }




    }
}
