import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.khomenko.easyTravel.Application;
import com.khomenko.easyTravel.service.AviationEdgeService;
import com.khomenko.easyTravel.service.GoogleMapsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class AviationEdgeServiceTest {

//    @TestConfiguration
//    static class AviatioEdgeServiceConfiguration{
//        @Bean
//        public AviationEdgeService getAviationEdgeService(){
//            AviationEdgeService aviationEdgeService=new AviationEdgeService();
//            return aviationEdgeService;
//        }
//    }

    @Autowired
    private AviationEdgeService aviationEdgeService;

    @Test
    public void getNearestAirport(){
        LatLng latLng=new LatLng(49.6766,21.7637);
        aviationEdgeService.getNearestAirportList(latLng);

        System.out.printf("");
    }
}
