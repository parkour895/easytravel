package com.khomenko.easyTravel.controller;


import com.khomenko.easyTravel.Application;
import com.khomenko.easyTravel.dto.own.Trip;
import com.khomenko.easyTravel.service.GoogleMapsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = Application.class)
@AutoConfigureMockMvc
public class BasicControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private GoogleMapsService googleMapsService;


    @Test
    public void getDataFromService(){

        try {
            mvc.perform(get("/good")).andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getNearestAirtport(){

        try {
            mvc.perform(get("/good")).andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Test
//    public void getOptimalWay(){
//
//        try {
//            mvc.perform(get("/optimalWay").requestAttr("trip",trip)).andExpect(status().isOk());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

}
