
import com.google.maps.errors.ApiException;
import com.khomenko.easyTravel.Application;
import com.khomenko.easyTravel.service.GoogleMapsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class GoogleMapsServiceTest {

//    @TestConfiguration
//    static class GoogleMapsServiceTestConfiguration{
//        @Bean
//        public GoogleMapsService getGoogleService(){
//            GoogleMapsService googleMapsService=new GoogleMapsService();
//            return googleMapsService;
//        }
//    }

    @Autowired
    private GoogleMapsService googleMapsService;

    @Test
    public void getDataFromService(){
        googleMapsService.getNearestAirport("Krosno");
    }

    @Test
    public void getNearestAirport() throws InterruptedException, ApiException, IOException {
        googleMapsService.getNearestAirportInPlacesApi("zaczernie");
    }

}
